from django.contrib import admin
from .models import Projects, Images, Videos, Files

# Register your models here.


class ProjectsAdmin(admin.ModelAdmin):
    list_display = ('id', 'projname', 'projuuid')


class ImagesAdmin(admin.ModelAdmin):
    list_display = ('id', 'project', 'imgpath')


class VideosAdmin(admin.ModelAdmin):
    list_display = ('id', 'project', 'videopath')


class FilesAdmin(admin.ModelAdmin):
    list_display = ('id', 'project', 'filepath')


admin.site.register(Projects, ProjectsAdmin)
admin.site.register(Images, ImagesAdmin)
admin.site.register(Videos, VideosAdmin)
admin.site.register(Files, FilesAdmin)
