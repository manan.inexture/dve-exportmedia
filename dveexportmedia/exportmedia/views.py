from django.shortcuts import render
from .models import Projects, Images, Videos, Files
from dveexportmedia import settings
from djqscsv import write_csv
from django.http import HttpResponse
import pandas as pd
import os
import zipfile
import csv
import uuid

def index(request):
    projects = Projects.objects.all()
    proj_objs = [proj for proj in projects]

    # import os.path
    # SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
    # print(SITE_ROOT)
    # fantasy_zip = zipfile.ZipFile(r'C:\Users\Mahadev\Documents\archive.zip', 'w')
    #
    # for folder, subfolders, files in os.walk(r'C:\Users\Mahadev\Documents\Inexture\GitLab\dve-exportmedia\dveexportmedia\media\project 1'):
    #
    #     for file in files:
    #         fantasy_zip.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder, file), r'C:\Users\Mahadev\Documents\Inexture\GitLab\dve-exportmedia\dveexportmedia\media\project 1'),compress_type=zipfile.ZIP_DEFLATED)
    #
    # fantasy_zip.close()

    return render(request, 'exportmedia/index.html', {'proj_objs': proj_objs})


def exportproject(request):
    project_uuid = request.POST['proj_select']
    project_obj = Projects.objects.get(projuuid=project_uuid)
    project_path = os.path.join(settings.MEDIA_ROOT, project_uuid)
    # zipfile_path = "%s\exports\%s\%s" % (settings.BASE_DIR, project_obj.proj_name, project_uuid)
    # zipfile_path = settings.BASE_DIR+'\\'+project_obj.proj_name+"\\"+project_uuid
    zipfile_path = os.path.join(settings.BASE_DIR, 'exports', project_obj.projname)
    zipfile_path += '.zip'

    try:
        project_zip = zipfile.ZipFile(zipfile_path, 'w')
        for folder, subfolders, files in os.walk(project_path):
            for file in files:
                # print("Path :", os.path.join(folder, file))
                # print("Relational Path :", os.path.relpath(os.path.join(folder, "manan", file), project_path))

                # project_zip.write(file, "csv" + "\\" + os.path.basename(file))
                project_zip.write(os.path.join(folder, file), project_uuid + "\\" + os.path.relpath(os.path.join(folder, file), project_path),
                                  compress_type=zipfile.ZIP_DEFLATED)

    except (IOError, RuntimeError, FileNotFoundError):
        return HttpResponse(status=500)

    project_queryset = Projects.objects.filter(projname=project_obj.projname)
    image_queryset = Images.objects.filter(project=project_obj)
    video_queryset = Videos.objects.filter(project=project_obj)
    file_queryset = Files.objects.filter(project=project_obj)

    with open(os.path.join(settings.BASE_DIR, 'exports') + '\\' + project_obj.projname + '.csv',
              'wb') as porject_data_file:
        write_csv(project_queryset, porject_data_file)

    with open(os.path.join(settings.BASE_DIR, 'exports') + '\\' + 'Images.csv', 'wb') as image_data_file:
        write_csv(image_queryset, image_data_file)

    with open(os.path.join(settings.BASE_DIR, 'exports') + '\\' + 'Videos.csv', 'wb') as video_data_file:
        write_csv(video_queryset, video_data_file)

    with open(os.path.join(settings.BASE_DIR, 'exports') + '\\' + 'Files.csv', 'wb') as file_data_file:
        write_csv(file_queryset, file_data_file)

    project_zip.write(porject_data_file.name,
                      "csv" + "\\" + os.path.basename(porject_data_file.name))
    project_zip.write(image_data_file.name,
                      "csv" + "\\" + os.path.basename(image_data_file.name))
    project_zip.write(video_data_file.name,
                      "csv" + "\\" + os.path.basename(video_data_file.name))
    project_zip.write(file_data_file.name,
                      "csv" + "\\" + os.path.basename(file_data_file.name))

    project_zip.close()
    os.remove(porject_data_file.name)
    os.remove(image_data_file.name)
    os.remove(video_data_file.name)
    os.remove(file_data_file.name)

    projects = Projects.objects.all()
    proj_objs = [proj for proj in projects]

    return render(request, 'exportmedia/index.html', {'proj_objs': proj_objs})


def importproject(request):































    if request.method == 'GET':
        return render(request, 'importmedia/importmedia.html')
    else:
        upload_file = request.FILES['myfile']

        import zipfile

        upload_zip = zipfile.ZipFile(upload_file)
        for file in upload_zip.namelist():
            if not file.startswith('csv/'):

                upload_zip.extract(file, settings.MEDIA_ROOT)
            else:

                # for finfo in upload_zip.infolist():
                #     print(finfo)
                # ifile = upload_zip.open(file)
                # print(ifile.read())

                    # line_list = ifile.readlines()

                df = pd.read_csv(upload_zip.open(file))
                for index, row in df.iterrows():
                    column_list = list(row.keys())
                    if('projuuid' in column_list):
                        project = Projects.objects.create(projname=str(row['projname']), projuuid=uuid.UUID(row['projuuid']))
                        project.save()
                    elif('imgpath' in column_list):
                        proj_obj = Projects.objects.get(projuuid=uuid.UUID(row['project_id']))
                        image = Images.objects.create(project=proj_obj, imgpath=row['imgpath'])
                        image.save()
                    elif('videopath' in column_list):
                        proj_obj = Projects.objects.get(projuuid=uuid.UUID(row['project_id']))
                        video = Videos.objects.create(project=proj_obj, videopath=row['videopath'])
                        video.save()
                    else:
                        proj_obj = Projects.objects.get(projuuid=uuid.UUID(row['project_id']))
                        file = Files.objects.create(project=proj_obj, filepath=row['filepath'])
                        file.save()

                # csv_file = upload_zip.open(file, "r")
                # reader = csv.reader(csv_file)
                # print(list(reader))
                # for row in reader:
                #     print(row)

                # with open(file, 'r') as csvfile:
                #     reader = csv.reader(csvfile)
                #
                #     for row in reader:
                #         print(row)
        # for finfo in upload_zip.infolist():
        #     ifile = upload_zip.open(finfo)
        #     line_list = ifile.readlines()
        #     print(line_list)


        # fantasy_zip.extract('', 'C:\\Stories\\Fantasy')

        upload_zip.close()


        return render(request, 'importmedia/importmedia.html')
