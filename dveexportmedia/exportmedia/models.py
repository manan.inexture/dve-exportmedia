from django.db import models
import uuid

def get_upload_path(instance, filename):
    return "%s/images/%s" % (instance.project.projuuid, filename)


def get_video_upload_path(instance, filename):
    return "%s/videos/%s" % (instance.project.projuuid, filename)


def get_file_upload_path(instance, filename):
    return "%s/files/%s" % (instance.project.projuuid, filename)


def validate_video_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.mp3', '.mkv', '.mp4']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')


class Projects(models.Model):
    projname = models.CharField(max_length=100)
    projuuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.projname


class Images(models.Model):
    project = models.ForeignKey(Projects, on_delete=models.CASCADE, to_field='projuuid')
    imgpath = models.ImageField(upload_to=get_upload_path)


class Videos(models.Model):
    project = models.ForeignKey(Projects, on_delete=models.CASCADE, to_field='projuuid')
    videopath = models.FileField(upload_to=get_video_upload_path, validators=[validate_video_file_extension])


class Files(models.Model):
    project = models.ForeignKey(Projects, on_delete=models.CASCADE, to_field='projuuid')
    filepath = models.FileField(upload_to=get_file_upload_path, validators=[validate_file_extension])
