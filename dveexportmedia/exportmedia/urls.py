from django.urls import path, re_path
from .views import index, exportproject, importproject

app_name = 'export'

urlpatterns = [
    path('', index, name='index'),
    path('exportproject/', exportproject, name='exportproject'),
    path('importproject/', importproject, name='importproject'),

]
