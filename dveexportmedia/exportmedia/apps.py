from django.apps import AppConfig


class ExportmediaConfig(AppConfig):
    name = 'exportmedia'
